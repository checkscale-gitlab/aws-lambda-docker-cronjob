
# ! Uncomment this later

# variable "cronjob_allowed_secrets_arn" {
#   type = string
#   description = "Secret granted to cronjob"
# }

variable "lambda_function_name" {
  type        = string
  description = "Lambda function name to execute cronjob"
}

variable "lambda_description" {
  type        = string
  description = "Lambda function description to execute cronjob"
}

variable "lambda_env_variables" {
  type    = map(string)
  default = {}
}

variable "cronjob_expression" {
  type        = string
  description = "Cronjob expression"
}

variable "cronjob_name" {
  type        = string
  description = "Name of cronjob"
}
variable "cronjob_description" {
  type        = string
  description = "Cronjob description"
}

variable "cronjob_handler" {
  type        = string
  description = "Cronjob handler"
}

variable "cronjob_timeout" {
  type        = string
  description = "Cronjob timeout"
}

variable "cronjob_memory_size" {
  type        = number
  description = "Cronjob Memory size"
  default     = 128
}

variable "image_config_command" {
  type        = list(string)
  description = "The CMD for the docker image	"
}

variable "image_config_entry_point" {
  type        = list(string)
  description = "The ENTRYPOINT for the docker image	"
}

variable "image_config_working_directory" {
  type        = string
  description = "Working directory for the docker image"
  default     = null
}

variable "image_uri" {
  type        = string
  description = "URI of Docker image used for Lambda function"
}

variable "tags" {
  type        = map(string)
  description = "Tags attached to resouces"
  default     = {}
}

variable "target_id" {
  type        = string
  description = "Target_ID of aws cloudwatch event target"
}

variable "aws_lambda_permission_statement_id" {
  type        = string
  description = "StatmentID of Lambda permission resource"
}

variable "iam_role_name" {
  type        = string
  description = "IAM role assigned to lambda function"
}

variable "role_permissions_boundary" {
  type        = string
  description = "IAM policy assigned IAM role attached to lambda function"
}


variable "ecs_security_group_ids" {
  type        = list(string)
  description = "Security group apply to ECS also applys to lamdba function"
}

variable "private_subnet_ids" {
  type        = list(string)
  description = "Private_subnet_ids"
}
